# VueCase

#### 介绍
vue的一些小例子

#### 版本说明

vue的版本：Vue.js v2.5.17

JavaScript：ES6以上

html：h5

css：css3

浏览器：谷歌  80.0.3987.132 

#### 软件架构
软件架构说明

js文件夹为vue.js框架或者html文档对应的js文件；

img为html文档中用到的图片；


#### 安装教程

直接复制到本地，双击html文件即可查看。

#### 使用说明

暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
